var app = angular.module('weather', []);

app.controller('main', ['$scope', '$http', function($scope, $http){
	
	var hours = new Date().getHours();
	if( 7 < hours && hours < 19){
		$scope.moment = 'day';
	}else{
		$scope.moment = 'night';
	}

	$scope.ready = false;

	$scope.errorLocation = function (location){
		$scope.showWeather = false;
		$scope.ready = true;
		$scope.$apply();
		
		console.log(location);
	}

	$scope.successLocation = function(location) {
			
	    $http({
		  	method: 'GET',
			url: 'http://api.openweathermap.org/data/2.5/weather',
			params: {
				lat : location.coords.latitude,
				lon : location.coords.longitude,
				units : 'metric',
				appid : '87a5b7a2f33b316172bb02b881b7aa82' 	
			}		
		})
		.then(
			function successCallback(response) {
				$scope.weatherObj = response.data;

				$scope.temp = $scope.weatherObj.main.temp;
				$scope.desc = $scope.weatherObj.weather[0].main;
				$scope.iconRaw = response.data.weather[0].icon ;
				$scope.iconTime = $scope.iconRaw.substr(2,1);
				$scope.icon = $scope.iconRaw.substr(0,2);
				$scope.city = $scope.weatherObj.name;

				$scope.showWeather = true;
				$scope.ready = true;
				

		    	console.log(response);
		    }, 
		  	function errorCallback(response) {
		    	console.log(response)
		    });

		
	}

	navigator.geolocation.getCurrentPosition($scope.successLocation, $scope.errorLocation); 
	
	
	$scope.searchByCity = function(searchValue){
		
		$scope.showWeather = true;
		$scope.ready = false;

		$http({
		  	method: 'GET',
			url: 'http://api.openweathermap.org/data/2.5/weather',
			params: {
				q : searchValue,
				units : 'metric', 
				appid : '87a5b7a2f33b316172bb02b881b7aa82' 	
			}		
		})
		.then(
			function successCallback(response) {
				$scope.weatherObj = response.data;

				$scope.temp = $scope.weatherObj.main.temp;
				$scope.desc = $scope.weatherObj.weather[0].main;
				$scope.iconRaw = response.data.weather[0].icon ;
				$scope.icon = $scope.iconRaw.substr(0,2);
				$scope.iconTime = $scope.iconRaw.substr(2,1);
				$scope.city = $scope.weatherObj.name;


				$scope.ready = true;

		    	console.log(response);
		    }, 
		  	function errorCallback(response) {
		    	console.log(response)
		    });
		
	}


}])

